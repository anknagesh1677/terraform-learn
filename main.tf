provider "aws" {

}

#variable "subnet_cidr_block" {
#  description = "subnet cidr block"
#}

variable "vpc_cidr_block" {
  description = "vpc cidr block"
}

variable "cidr_blocks" {
  description = "cidr block values for all"
  #default = "10.0.0.0/24"
  type = list(string)
  #type = list (object ({
  #  cidr_blocks = string
  #  name = string
  #}))
}

variable "env_vars" {
  description = "deployment variables"
}

resource "aws_vpc" "dev_vpc" {
  cidr_block = var.cidr_blocks[0]
  tags = {
    Name: "dev_vpc"
  }
}

resource "aws_subnet" "dev_subnet_1" {
  vpc_id = aws_vpc.dev_vpc.id
  cidr_block = var.cidr_blocks[1]
  availability_zone = "us-east-1a"
  tags = {
    Name: "dev_subnet_1"
    Env: var.env_vars
  }
}

data "aws_vpc" "existing_vpc" {
  default = true
}

resource "aws_subnet" "dev_subnet_2" {
  vpc_id = data.aws_vpc.existing_vpc.id
  cidr_block = var.vpc_cidr_block
  availability_zone = "us-east-1b"
  tags = {
    Name: "dev_subnet_2"
  }
}

output "dev_vpc_id" {
  value = aws_vpc.dev_vpc.id
}

output "dev_subnet_id" {
  value = aws_subnet.dev_subnet_2.id
}